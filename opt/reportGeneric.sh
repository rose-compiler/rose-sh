#!/bin/bash
#
# Script to generate an error report for an application that was run by compileGeneric.sh.
# It takes one argument: The name of the application.
#

NAME=$1
OUTFILE=$2

OUT_FOLDER=${PWD}

echo "*** Stats for" $NAME "in" $OUTFILE:
echo
count=`grep "SUCCESS compiling" $OUTFILE | wc -l`                                    &&  echo "    " $NAME "SUCCESS:" $count
count=`grep "ERROR compiling"   $OUTFILE | wc -l`                                    &&  echo "    " $NAME "ERROR:" $count
count=`grep "Syntax errors detected in input java program" $OUTFILE | wc -l`         &&  echo "    " $NAME "JAVAC Syntax errors: " $count
count=`grep "ECJ front-end errors detected in input java program" $OUTFILE | wc -l`  &&  echo "    " $NAME "ECJ front-end errors: " $count
count=`grep "No support" $OUTFILE | wc -l`                                           &&  echo "    " $NAME "Unsupported features:" $count
count=`grep "At least one non-ASCII" $OUTFILE | wc -l`                               &&  echo "    " $NAME "Illegal wide character:" $count
count=`grep "java.lang.ClassNotFoundException" $OUTFILE | wc -l`                     &&  echo "    " $NAME "Class Not Found Exception:" $count
count=`grep "Verification error detected" $OUTFILE | wc -l`                          &&  echo "    " $NAME "Verification Error:" $count
count=`grep "ERROR found in output file" $OUTFILE | wc -l`                           &&  echo "    " $NAME "Recompilation Error:" $count
echo
count=`grep "ROSE bug" $OUTFILE | wc -l`                                             &&  echo "    " $NAME "ROSE bug:" $count
count=`grep "Inserting package" $OUTFILE | wc -l`                                    &&  echo "    " $NAME "Number of packages:" $count
echo
grep -E "ERROR compiling|Syntax errors detected in input java program|ECJ front-end errors|ROSE bug|No support|At least one non-ASCII|java.lang.ClassNotFoundException|Verification error detected|ERROR found in output file" $OUTFILE >& $OUTFILE.err
