: ${APACHE_LENYA_DEPENDENCIES:=apache_ant ss_ant_rose}
: ${APACHE_LENYA_CONFIGURE_OPTIONS:=
    -Dcom.pontetec.rosecompiler.use_single_commandline="true"
    -Dcom.pontetec.rosecompiler.translator.arg.rose.skip_commentsAndDirectives=""
  }
: ${APACHE_LENYA_ANT_TARGET:=webapp}

#-------------------------------------------------------------------------------
download_apache_lenya()
#-------------------------------------------------------------------------------
{
  info "Downloading source code"

  set -x
      clone_repository "${application}" "${application}-src" || exit 1
      cd "${application}-src/" || exit 1
  set +x
}

#-------------------------------------------------------------------------------
install_deps_apache_lenya()
#-------------------------------------------------------------------------------
{
  install_deps ${APACHE_LENYA_DEPENDENCIES} || fail "Could not install dependencies"
}

#-------------------------------------------------------------------------------
patch_apache_lenya()
#-------------------------------------------------------------------------------
{
  info "Patching not required"
}

#-------------------------------------------------------------------------------
configure_apache_lenya__rose()
#-------------------------------------------------------------------------------
{
  info "Compiling application for ROSE compiler='${ROSE_CC}'"

  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
      #./build.sh || fail "An error occurred during application bootstrapping"

      # TOO1 (1/30/2014): Possible workaround for Philippe's error:
      #
      #     init-build.xml:82: It seems that your externals/cocoon_2_1_x/local.blocks.properties is NOT in sync with src/cocoon/local.blocks.properties! The file
      #     src/cocoon/local.blocks.properties has probably been modified by some Apache Lenya developer. Issue a 'build clean-all' and rebuild Lenya.
      ant clean-all || fail "An error occurred during 'ant clean-all'"

      KG__STRIP_PATH="$(pwd)/" \
      KG__REPORT_FAIL="$(pwd)/rose-fails.txt" \
      KG__REPORT_PASS="$(pwd)/rose-passes.txt" \
        /usr/bin/time --format='%E' \
            ant "${APACHE_LENYA_ANT_TARGET}" \
                -verbose \
                -lib "${ROSE_SH_DEPS_PREFIX}/lib" \
                -Dbuild.compiler="com.pontetec.RoseCompilerAdapter" \
                -Dcom.pontetec.rosecompiler.translator="${ROSE_CC}" \
                ${APACHE_LENYA_CONFIGURE_OPTIONS} \
        || fail "An error occurred during application compilation"
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------
}

#-------------------------------------------------------------------------------
configure_apache_lenya__gcc()
#-------------------------------------------------------------------------------
{
  info "Configuring application for default compiler='${CC}'"

  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
    #./build.sh || fail "An error occurred during application bootstrapping"

    /usr/bin/time --format='%E' \
        ant "${APACHE_LENYA_ANT_TARGET}" \
            ${APACHE_LENYA_CONFIGURE_OPTIONS} \
    || fail "An error occurred during application compilation"
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------
}

#-------------------------------------------------------------------------------
compile_apache_lenya()
#-------------------------------------------------------------------------------
{
  info "Application compilation was performed during the configure stage"
}
