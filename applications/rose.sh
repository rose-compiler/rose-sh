: ${ROSE_DEPENDENCIES:=boost147}
: ${ROSE_CONFIGURE_OPTIONS:=
  --with-ROSE_LONG_MAKE_CHECK_RULE
  }

#-------------------------------------------------------------------------------
download_rose()
#-------------------------------------------------------------------------------
{
  info "Downloading source code"

  set -x
      clone_repository "${application}" "${application}-src" || exit 1
      cd "${application}-src/" || exit 1

      # TOO1 (2/7/2014) Use submodule source code if possible
      git submodule update --init || true
  set +x
}

#-------------------------------------------------------------------------------
install_deps_rose()
#-------------------------------------------------------------------------------
{
  install_deps ${ROSE_DEPENDENCIES} || fail "Could not install dependencies"
}

#-------------------------------------------------------------------------------
patch_rose()
#-------------------------------------------------------------------------------
{
  info "Patching not required"
}

#-------------------------------------------------------------------------------
configure_rose__rose()
#-------------------------------------------------------------------------------
{
  info "Configuring application for ROSE compiler CC='${ROSE_CC}' CXX='${ROSE_CXX}'"

  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
      ./build || fail "An error occurred during application bootstrapping"

      mkdir -p "build_tree"   || fail "Could not create application build tree"
      pushd "build_tree/"     || fail "Could not change into application build tree"
          CC="${ROSE_CC}" \
          CXX="${ROSE_CXX}" \
              ../configure \
                  --prefix="$(pwd)/install_tree" \
                  --with-boost="${ROSE_SH_DEPS_PREFIX}" \
                  ${ROSE_CONFIGURE_OPTIONS} || fail "An error occurred during application configuration"
      popd
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------
}

#-------------------------------------------------------------------------------
configure_rose__gcc()
#-------------------------------------------------------------------------------
{
  info "Configuring application for default compiler CC='${CC}' CXX='${CXX}'"

  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
      ./build || fail "An error occurred during application bootstrapping"

      mkdir -p "build_tree"   || fail "Could not create application build tree"
      pushd "build_tree/"     || fail "Could not change into application build tree"
          CC="${CC}" \
          CXX="${CXX}" \
              ../configure \
                  --prefix="$(pwd)/install_tree" \
                  --with-boost="${ROSE_SH_DEPS_PREFIX}" \
                  ${ROSE_CONFIGURE_OPTIONS} || fail "An error occurred during application configuration"
      popd
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------
}

#-------------------------------------------------------------------------------
compile_rose()
#-------------------------------------------------------------------------------
{
  info "Compiling application"

  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
      pushd "build_tree/"           || fail "Could not change into application build tree"
      make -j${parallelism}         || fail "An error occurred during application compilation"
      make -j${parallelism} install || fail "An error occurred during application installation"
      popd
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------
}
