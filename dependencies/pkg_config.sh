: ${PKG_CONFIG_DEPENDENCIES:=}
: ${PKG_CONFIG_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${PKG_CONFIG_TARBALL:="pkg-config-0.28.tar.gz"}
: ${PKG_CONFIG_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/share/aclocal//pkg.m4"}

#-------------------------------------------------------------------------------
install_pkg_config()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${PKG_CONFIG_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${PKG_CONFIG_INSTALLED_FILE}" ]; then
      rm -rf "./pkg_config"                           || fail "Unable to remove application workspace"
      mkdir -p "pkg_config"                           || fail "Unable to create application workspace"
      cd "pkg_config/"                                || fail "Unable to change into the application workspace"

      download_tarball "${PKG_CONFIG_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${PKG_CONFIG_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${PKG_CONFIG_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      ./configure ${PKG_CONFIG_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] pkg_config is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
