: ${BINUTILS_DEPENDENCIES:=}
: ${BINUTILS_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${BINUTILS_TARBALL:="binutils-2.23.0.tar.bz2"}
: ${BINUTILS_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/share/info/binutils.info"}

#-------------------------------------------------------------------------------
install_binutils()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${BINUTILS_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${BINUTILS_INSTALLED_FILE}" ]; then
      rm -rf "./binutils"                           || fail "Unable to remove application workspace"
      mkdir -p "binutils"                           || fail "Unable to create application workspace"
      cd "binutils/"                                || fail "Unable to change into the application workspace"

      download_tarball "${BINUTILS_TARBALL}"        || fail "Unable to download application tarball"
      tar xjvf "${BINUTILS_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${BINUTILS_TARBALL%.tar.bz2})" || fail "Unable to change into application source directory"

      ./configure ${BINUTILS_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                         || fail "An error occurred during application compilation"
      make -j${parallelism} install                 || fail "An error occurred during application installation"
  else
      info "[SKIP] binutils is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
