# http://cgit.freedesktop.org/xorg/proto/renderproto/

: ${RENDERPROTO_DEPENDENCIES:=xorg_util_macros}
: ${RENDERPROTO_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${RENDERPROTO_TARBALL:="renderproto-0.11.1.tar.gz"}
: ${RENDERPROTO_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/X11/extensions/render.h"}

#-------------------------------------------------------------------------------
install_renderproto()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${RENDERPROTO_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${RENDERPROTO_INSTALLED_FILE}" ]; then
      rm -rf "./renderproto"                           || fail "Unable to remove application workspace"
      mkdir -p "renderproto"                           || fail "Unable to create application workspace"
      cd "renderproto/"                                || fail "Unable to change into the application workspace"

      download_tarball "${RENDERPROTO_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${RENDERPROTO_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${RENDERPROTO_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      # TOO1 (2/12/2014): xorg-macros.m4 must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ./autogen.sh                                || fail "Unable to bootstrap application"
      ./configure ${RENDERPROTO_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] renderproto is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
