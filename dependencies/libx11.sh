# http://cgit.freedesktop.org/xorg/lib/libX11/

: ${LIBX11_DEPENDENCIES:=xorg_util_macros libxmu}
: ${LIBX11_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${LIBX11_TARBALL:="libX11-1.6.2.tar.gz"}
: ${LIBX11_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/lib/pkgconfig/xrender.pc"}

#-------------------------------------------------------------------------------
install_libx11()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${LIBX11_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${LIBX11_INSTALLED_FILE}" ]; then
      rm -rf "./libx11"                           || fail "Unable to remove application workspace"
      mkdir -p "libx11"                           || fail "Unable to create application workspace"
      cd "libx11/"                                || fail "Unable to change into the application workspace"

      download_tarball "${LIBX11_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${LIBX11_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${LIBX11_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      # TOO1 (2/12/2014): xorg-macros.m4 must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ./autogen.sh                                || fail "Unable to bootstrap application"
      ./configure ${LIBX11_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] libx11 is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
