: ${HUNSPELL_DEPENDENCIES:=readline ncurses}
: ${HUNSPELL_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
    --enable-threads=posix
    --with-ui
    --with-readline
  }
: ${HUNSPELL_TARBALL:="hunspell-1.3.2.tar.gz"}
: ${HUNSPELL_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/hunspell/hunspell.h"}

#-------------------------------------------------------------------------------
install_hunspell()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${HUNSPELL_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${HUNSPELL_INSTALLED_FILE}" ]; then
      rm -rf "./hunspell"                           || fail "Unable to remove application workspace"
      mkdir -p "hunspell"                           || fail "Unable to create application workspace"
      cd "hunspell/"                                || fail "Unable to change into the application workspace"

      download_tarball "${HUNSPELL_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${HUNSPELL_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${HUNSPELL_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      ./configure ${HUNSPELL_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] hunspell is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
