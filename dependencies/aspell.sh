# command=LDFLAGS="$LDFLAGS $SS_LD_COPY_DT_NEEDED" ./configure --prefix="$STONESOUP_INSTALL_PREFIX" --libdir="$SS_LIBDIR" 

: ${ASPELL_DEPENDENCIES:=ncursesw}
: ${ASPELL_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
    --enable-static
  }
: ${ASPELL_TARBALL:="aspell-0.60.6.1.tar.gz"}
: ${ASPELL_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/aspell/aspell.h"}

#-------------------------------------------------------------------------------
install_aspell()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${ASPELL_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${ASPELL_INSTALLED_FILE}" ]; then
      rm -rf "./aspell"                           || fail "Unable to remove application workspace"
      mkdir -p "aspell"                           || fail "Unable to create application workspace"
      cd "aspell/"                                || fail "Unable to change into the application workspace"

      download_tarball "${ASPELL_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${ASPELL_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${ASPELL_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      ./configure ${ASPELL_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] aspell is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
