# http://cgit.freedesktop.org/xorg/lib/libXt/

: ${LIBXT_DEPENDENCIES:=xorg_util_macros}
: ${LIBXT_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${LIBXT_TARBALL:="libXt-1.1.4.tar.gz"}
: ${LIBXT_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/lib/pkgconfig/libxtt.pc"}

#-------------------------------------------------------------------------------
install_libxt()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${LIBXT_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${LIBXT_INSTALLED_FILE}" ]; then
      rm -rf "./libxt"                           || fail "Unable to remove application workspace"
      mkdir -p "libxt"                           || fail "Unable to create application workspace"
      cd "libxt/"                                || fail "Unable to change into the application workspace"

      download_tarball "${LIBXT_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${LIBXT_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${LIBXT_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      # TOO1 (2/7/2014): xorg-macros.m4 must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ./autogen.sh                                || fail "Unable to bootstrap application"
      ./configure ${LIBXT_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] libxt is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
