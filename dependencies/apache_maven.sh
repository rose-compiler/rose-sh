# TOO1 (2/11/2014): Interferes with applications/apache_maven.sh
#: ${APACHE_MAVEN_DEPENDENCIES:=}
: ${APACHE_MAVEN_TARBALL:="apache-maven-3.1.1-bin.tar.gz"}
: ${APACHE_MAVEN_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/bin/mvn"}

#-------------------------------------------------------------------------------
install_apache_maven()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
# TOO1 (2/11/2014): Interferes with applications/apache_maven.sh
#  install_deps ${APACHE_MAVEN_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${APACHE_MAVEN_INSTALLED_FILE}" ]; then
      rm -rf "./apache_maven"                           || fail "Unable to create application workspace"
      mkdir -p "apache_maven"                           || fail "Unable to create application workspace"
      cd "apache_maven/"                                || fail "Unable to change into the application workspace"

      download_tarball "${APACHE_MAVEN_TARBALL}"            || fail "Unable to download application tarball"
      tar xzvf "${APACHE_MAVEN_TARBALL}"                    || fail "Unable to unpack application tarball"
      cd "$(basename ${APACHE_MAVEN_TARBALL%-bin.tar.gz})"  || fail "Unable to change into application source directory"

      mkdir -p "${ROSE_SH_DEPS_PREFIX}/bin"   || fail "Unable to create ${ROSE_SH_DEPS_PREFIX}/bin"
      mkdir -p "${ROSE_SH_DEPS_PREFIX}/boot"  || fail "Unable to create ${ROSE_SH_DEPS_PREFIX}/bin"
      mkdir -p "${ROSE_SH_DEPS_PREFIX}/conf"  || fail "Unable to create ${ROSE_SH_DEPS_PREFIX}/bin"
      mkdir -p "${ROSE_SH_DEPS_PREFIX}/lib"   || fail "Unable to create ${ROSE_SH_DEPS_PREFIX}/lib"

      cp -r bin/* "${ROSE_SH_DEPS_PREFIX}/bin"    || fail "Unable to install application binaries"
      cp -r boot/* "${ROSE_SH_DEPS_PREFIX}/boot"  || fail "Unable to install application binaries"
      cp -r conf/* "${ROSE_SH_DEPS_PREFIX}/conf"  || fail "Unable to install application binaries"
      cp -r lib/* "${ROSE_SH_DEPS_PREFIX}/lib"    || fail "Unable to install application lib files"
  else
      info "[SKIP] apache_maven is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
