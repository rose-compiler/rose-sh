: ${ENCHANT_DEPENDENCIES:=hunspell aspell zlib glib}
: ${ENCHANT_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
    --enable-hspell
    --enable-aspell
    --disable-ispell
    --disable-myspell
    --disable-voikko
    --disable-uspell
    --disable-zemberek
    --with-aspell-prefix="${ROSE_SH_DEPS_PREFIX}"
    --with-hspell-prefix="${ROSE_SH_DEPS_PREFIX}"
  }
: ${ENCHANT_TARBALL:="enchant-1.6.0.tar.gz"}
: ${ENCHANT_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/enchant/enchant.h"}

#-------------------------------------------------------------------------------
install_enchant()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${ENCHANT_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${ENCHANT_INSTALLED_FILE}" ]; then
      rm -rf "./enchant"                           || fail "Unable to remove application workspace"
      mkdir -p "enchant"                           || fail "Unable to create application workspace"
      cd "enchant/"                                || fail "Unable to change into the application workspace"

      download_tarball "${ENCHANT_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${ENCHANT_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${ENCHANT_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      ./configure ${ENCHANT_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] enchant is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
