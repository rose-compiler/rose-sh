: ${GTK389_DEPENDENCIES:=glib atk pango cairo gdk_pixbuf}
: ${GTK389_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
    --disable-cups
    --disable-papi
    --with-x
    --enable-x11-backend
    --enable-broadway-backend
  }
: ${GTK389_TARBALL:="gtk+-3.8.9.tar.xz"}
: ${GTK389_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/gtk389-1.0/gtk389/gtk389.h"}

#-------------------------------------------------------------------------------
install_gtk389()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${GTK389_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${GTK389_INSTALLED_FILE}" ]; then
      rm -rf "./gtk389"  || fail "Unable to remove application workspace"
      mkdir -p "gtk389"  || fail "Unable to create application workspace"
      cd "gtk389/"       || fail "Unable to change into the application workspace"

      download_tarball "${GTK389_TARBALL}"       || fail "Unable to download application tarball"
      unxz "${GTK389_TARBALL}"                   || fail "Unable to unpack application tarball"
      tar xvf "${GTK389_TARBALL%.xz}"            || fail "Unable to unpack application tarball"
      cd "$(basename ${GTK389_TARBALL%.tar.xz})" || fail "Unable to change into application source directory"

      ./configure ${GTK389_CONFIGURE_OPTIONS} || fail "Unable to configure application"

      make -j${parallelism}         || fail "An error occurred during application compilation"
      make -j${parallelism} install || fail "An error occurred during application installation"
  else
      info "[SKIP] gtk389 is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
