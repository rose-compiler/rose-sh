# http://cgit.freedesktop.org/xorg/lib/libXext/

: ${LIBXMU_DEPENDENCIES:=xorg_util_macros}
: ${LIBXMU_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${LIBXMU_TARBALL:="libXmu-1.1.2.tar.gz"}
: ${LIBXMU_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/lib/pkgconfig/xmu.pc"}

#-------------------------------------------------------------------------------
install_libxmu()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${LIBXMU_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${LIBXMU_INSTALLED_FILE}" ]; then
      rm -rf "./libxmu"                           || fail "Unable to remove application workspace"
      mkdir -p "libxmu"                           || fail "Unable to create application workspace"
      cd "libxmu/"                                || fail "Unable to change into the application workspace"

      download_tarball "${LIBXMU_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${LIBXMU_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${LIBXMU_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      # TOO1 (2/7/2014): xorg-macros.m4 must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ./autogen.sh                                || fail "Unable to bootstrap application"
      ./configure ${LIBXMU_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] libxmu is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
