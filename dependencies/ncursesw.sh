# TOO1 (2/18/2014): May need to clear CFLAGS and CPPFLAGS if conflicting with ncurses.

: ${NCURSESW_DEPENDENCIES:=}
: ${NCURSESW_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --exec-prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_PREFIX}/lib"
    --enable-pc-files
    --disable-overwrite
    --enable-widec
    --with-shared
    --with-termlib=tinfo
    --enable-rpath
    --disable-termcap
    --enable-symlinks
    --with-ticlib=tic
    --without-ada
    --without-tests
    --without-profile
    --without-debug
    --enable-echo
    --with-progs
    --enable-const
  }
: ${NCURSESW_TARBALL:="ncurses-5.9.tar.gz"}
: ${NCURSESW_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/ncursesw/ncursesw.h"}

#-------------------------------------------------------------------------------
install_ncursesw()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${NCURSESW_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${NCURSESW_INSTALLED_FILE}" ]; then
      rm -rf "./ncursesw"                           || fail "Unable to remove application workspace"
      mkdir -p "ncursesw"                           || fail "Unable to create application workspace"
      cd "ncursesw/"                                || fail "Unable to change into the application workspace"

      download_tarball "${NCURSESW_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${NCURSESW_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${NCURSESW_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      ./configure ${NCURSESW_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] ncursesw is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
