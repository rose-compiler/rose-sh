# http://cgit.freedesktop.org/xorg/proto/xproto/

: ${XPROTO_DEPENDENCIES:=xorg_util_macros}
: ${XPROTO_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${XPROTO_TARBALL:="xproto-7.0.25.tar.gz"}
: ${XPROTO_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/lib/pkgconfig/xproto.pc"}

#-------------------------------------------------------------------------------
install_xproto()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${XPROTO_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${XPROTO_INSTALLED_FILE}" ]; then
      rm -rf "./xproto"                           || fail "Unable to remove application workspace"
      mkdir -p "xproto"                           || fail "Unable to create application workspace"
      cd "xproto/"                                || fail "Unable to change into the application workspace"

      download_tarball "${XPROTO_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${XPROTO_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${XPROTO_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      # TOO1 (2/7/2014): xorg-macros.m4 must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ./autogen.sh                                || fail "Unable to bootstrap application"
      ./configure ${XPROTO_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] xproto is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
