# http://cgit.freedesktop.org/xorg/lib/libXau/

: ${LIBXAU_DEPENDENCIES:=xorg_util_macros xproto}
: ${LIBXAU_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${LIBXAU_TARBALL:="libXau-1.0.8.tar.gz"}
: ${LIBXAU_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/lib/pkgconfig/xau.pc"}

#-------------------------------------------------------------------------------
install_libxau()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${LIBXAU_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${LIBXAU_INSTALLED_FILE}" ]; then
      rm -rf "./libxau"                           || fail "Unable to remove application workspace"
      mkdir -p "libxau"                           || fail "Unable to create application workspace"
      cd "libxau/"                                || fail "Unable to change into the application workspace"

      download_tarball "${LIBXAU_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${LIBXAU_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${LIBXAU_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      # TOO1 (2/7/2014): xorg-macros.m4 must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ./autogen.sh                                || fail "Unable to bootstrap application"
      ./configure ${LIBXAU_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] libxau is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
