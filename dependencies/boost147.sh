: ${BOOST147_DEPENDENCIES:=}
: ${BOOST147_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${BOOST147_TARBALL:="boost_1_47_0.tar.gz"}
: ${BOOST147_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/boost/version.hpp"}

#-------------------------------------------------------------------------------
install_boost147()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${BOOST147_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${BOOST147_INSTALLED_FILE}" ]; then
      rm -rf "./boost147"                            || fail "Unable to create application workspace"
      mkdir -p "boost147"                            || fail "Unable to create application workspace"
      cd "boost147/"                                 || fail "Unable to change into the application workspace"

      download_tarball "${BOOST147_TARBALL}"         || fail "Unable to download application tarball"
      tar xzvf "${BOOST147_TARBALL}"                 || fail "Unable to unpack application tarball"
      cd "$(basename ${BOOST147_TARBALL%.tar.gz})"   || fail "Unable to change into application source directory"

      ./bootstrap.sh \
          --prefix="${ROSE_SH_DEPS_PREFIX}" || fail "Unable to configure application"

      local BOOST147_BUILD_CMD=
      if [ -e "./bjam" ]; then
          BOOST147_BUILD_CMD="bjam"
      elif [ -e "./b2" ]; then
          BOOST147_BUILD_CMD="b2"
      else
        fail "boost147 build script not found (bjam/b2)"
      fi

      "./${BOOST147_BUILD_CMD}" install \
          -j${parallelism} \
          ${BOOST147_CONFIGURE_OPTIONS} || fail "Unable to compile and install application"
  else
      info "[SKIP] boost147 is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
