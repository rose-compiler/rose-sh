: ${GEGL_DEPENDENCIES:=intltool}
: ${GEGL_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${GEGL_TARBALL:="gegl-0.2.0.tar.bz2"}
: ${GEGL_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/gegl-0.2/gegl.h"}

#-------------------------------------------------------------------------------
install_gegl()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${GEGL_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${GEGL_INSTALLED_FILE}" ]; then
      rm -rf "./gegl"  || fail "Unable to remove application workspace"
      mkdir -p "gegl"  || fail "Unable to create application workspace"
      cd "gegl/"       || fail "Unable to change into the application workspace"

      download_tarball "${GEGL_TARBALL}"        || fail "Unable to download application tarball"
      tar xjvf "${GEGL_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${GEGL_TARBALL%.tar.bz2})" || fail "Unable to change into application source directory"

      # TOO1 (2/26/2014): This may work more elegantly: CFLAGS='-std=gnu99'
      CFLAGS="-DNAN=\"(0.0/0.0)\"" \
        ./configure ${GEGL_CONFIGURE_OPTIONS} || fail "Unable to configure application"

      make -j${parallelism}         || fail "An error occurred during application compilation"
      make -j${parallelism} install || fail "An error occurred during application installation"
  else
      info "[SKIP] gegl is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
