# http://cgit.freedesktop.org/xorg/lib/libXrender/

: ${LIBXRENDER_DEPENDENCIES:=xorg_util_macros renderproto libx11}
: ${LIBXRENDER_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${LIBXRENDER_TARBALL:="libXrender-0.9.8.tar.gz"}
: ${LIBXRENDER_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/lib/pkgconfig/xrender.pc"}

#-------------------------------------------------------------------------------
install_libxrender()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${LIBXRENDER_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${LIBXRENDER_INSTALLED_FILE}" ]; then
      rm -rf "./libxrender"                           || fail "Unable to remove application workspace"
      mkdir -p "libxrender"                           || fail "Unable to create application workspace"
      cd "libxrender/"                                || fail "Unable to change into the application workspace"

      download_tarball "${LIBXRENDER_TARBALL}"        || fail "Unable to download application tarball"
      tar xzvf "${LIBXRENDER_TARBALL}"                || fail "Unable to unpack application tarball"
      cd "$(basename ${LIBXRENDER_TARBALL%.tar.gz})"  || fail "Unable to change into application source directory"

      # TOO1 (2/12/2014): xorg-macros.m4 must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ./autogen.sh                                || fail "Unable to bootstrap application"
      ./configure ${LIBXRENDER_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] libxrender is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
