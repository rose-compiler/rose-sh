# http://cgit.freedesktop.org/xorg/lib/libXi/

: ${LIBXI_DEPENDENCIES:=xorg_util_macros}
: ${LIBXI_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
  }
: ${LIBXI_TARBALL:="libXi-1.7.2.tar.gz"}
: ${LIBXI_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/include/libxi/libxi.h"}

#-------------------------------------------------------------------------------
install_libxi()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${LIBXI_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${LIBXI_INSTALLED_FILE}" ]; then
      rm -rf "./libxi"                            || fail "Unable to remove application workspace"
      mkdir -p "libxi"                            || fail "Unable to create application workspace"
      cd "libxi/"                                 || fail "Unable to change into the application workspace"

      download_tarball "${LIBXI_TARBALL}"         || fail "Unable to download application tarball"
      tar xzvf "${LIBXI_TARBALL}"                 || fail "Unable to unpack application tarball"
      cd "$(basename ${LIBXI_TARBALL%.tar.gz})"   || fail "Unable to change into application source directory"

      # TOO1 (2/7/2014): Append definining variable declaration to fix this autogen.sh error:
      #
      #                     man/Makefile.am:161: error: MAN_SUBSTS must be set with '=' before using '+='
      cp man/Makefile.am man/Makefile.am-original
      echo 'MAN_SUBSTS=' | cat - man/Makefile.am > man/Makefile.am-patched &&
          cp man/Makefile.am-patched man/Makefile.am || fail "Unable to patch application"

      # TOO1 (2/7/2014): xorg-macros.m4, libtool.m4, pkg.m4 (pkg-config) must be found in the ACLOCAL_PATH.
      ACLOCAL_PATH="${ROSE_SH_DEPS_PREFIX}/share/aclocal:${ACLOCAL_PATH}" \
      ACLOCAL_PATH="${LIBTOOL_HOME}/share/aclocal:${ACLOCAL_PATH}" \
      ACLOCAL_PATH="/usr/share/aclocal:${ACLOCAL_PATH}" \
          ./autogen.sh                            || fail "Unable to bootstrap application"
      ./configure ${LIBXI_CONFIGURE_OPTIONS}      || fail "Unable to configure application"

      make -j${parallelism}                       || fail "An error occurred during application compilation"
      make -j${parallelism} install               || fail "An error occurred during application installation"
  else
      info "[SKIP] libxi is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
