: ${GOFFICE_DEPENDENCIES:=glib gtk324 pango libxml2 atk libgsf cairo librsvg}
: ${GOFFICE_CONFIGURE_OPTIONS:=
    --prefix="${ROSE_SH_DEPS_PREFIX}"
    --libdir="${ROSE_SH_DEPS_LIBDIR}"
    --enable-static
    --with-gtk
    --with-lasem=no
  }
: ${GOFFICE_TARBALL:="goffice-0.10.8.tar.xz"}
: ${GOFFICE_INSTALLED_FILE:="${ROSE_SH_DEPS_PREFIX}/lib/goffice/0.10.8/plugins/plot_xy/xy.a"}

#-------------------------------------------------------------------------------
install_goffice()
#-------------------------------------------------------------------------------
{
  info "Installing application"

  #-----------------------------------------------------------------------------
  rosesh__install_dep_setup || exit 1
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Dependencies
  #-----------------------------------------------------------------------------
  install_deps ${GOFFICE_DEPENDENCIES} || exit 1

  #-----------------------------------------------------------------------------
  # Installation
  #-----------------------------------------------------------------------------
  set -x
  #-----------------------------------------------------------------------------
  if [ ! -f "${GOFFICE_INSTALLED_FILE}" ]; then
      rm -rf "./goffice"                            || fail "Unable to remove application workspace"
      mkdir -p "goffice"                            || fail "Unable to create application workspace"
      cd "goffice/"                                 || fail "Unable to change into the application workspace"

      download_tarball "${GOFFICE_TARBALL}"         || fail "Unable to download application tarball"
      unxz "${GOFFICE_TARBALL}"                     || fail "Unable to unpack application tarball"
      tar xvf "${GOFFICE_TARBALL%.xz}"              || fail "Unable to unpack application tarball"
      cd "$(basename ${GOFFICE_TARBALL%.tar.xz})"   || fail "Unable to change into application source directory"

      ./configure ${GOFFICE_CONFIGURE_OPTIONS}     || fail "Unable to configure application"

      make -j${parallelism}                     || fail "An error occurred during application compilation"
      make -j${parallelism} install             || fail "An error occurred during application installation"
  else
      info "[SKIP] goffice is already installed"
  fi
  #-----------------------------------------------------------------------------
  set +x
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  rosesh__install_dep_teardown || exit 1
  #-----------------------------------------------------------------------------
}
